import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { LoggerService } from '../services/logger.service';

@Injectable()
export class Interceptor2Interceptor implements NestInterceptor {
  constructor(private loggerService: LoggerService) {}

  intercept(context: ExecutionContext, next: CallHandler) {
    this.loggerService.incPad();
    this.loggerService.log('Interceptor2Interceptor => intercept()');
    // return of([1, 2, 3]);
    return next.handle().pipe(
      tap(data => {
        this.loggerService.logTapTypeof(data);
      }),
      catchError(err => {
        this.loggerService.descPad();
        this.loggerService.log('Interceptor2Interceptor => catchError =>', err);
        return throwError(err);
      }),
      tap(() => {
        this.loggerService.log('END Interceptor2Interceptor');
      }),
      tap(() => {
        this.loggerService.descPad();
      }),
    );
  }
}
