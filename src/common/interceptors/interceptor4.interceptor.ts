import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, tap, timeout } from 'rxjs/operators';
import { LoggerService } from '../services/logger.service';
import { throwError, TimeoutError } from 'rxjs';

@Injectable()
export class Interceptor4Interceptor implements NestInterceptor {
  constructor(private loggerService: LoggerService) {}

  intercept(context: ExecutionContext, next: CallHandler) {
    this.loggerService.incPad();
    this.loggerService.log('Interceptor4Interceptor => intercept()');

    return next.handle().pipe(
      tap(data => {
        this.loggerService.logTapTypeof(data);
      }),
      // timeout for the request, if it takes more than this,
      // then it will throw an error.
      // timeout(3000),
      catchError(err => {
        this.loggerService.descPad();
        this.loggerService.log('Interceptor4Interceptor => catchError =>', err);
        return throwError(err);
      }),
      tap(() => {
        this.loggerService.log('END Interceptor4Interceptor');
      }),
      tap(() => {
        this.loggerService.descPad();
      }),
    );
  }
}
