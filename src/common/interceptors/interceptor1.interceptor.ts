import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { LoggerService } from '../services/logger.service';

interface Response<T> {
  data: T;
}

@Injectable()
export class Interceptor1Interceptor<T>
  implements NestInterceptor<T, Response<T>> {
  constructor(private loggerService: LoggerService) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    this.loggerService.incPad();
    this.loggerService.log('Interceptor1Interceptor => intercept()');

    // this calls the next interceptor (if any, otherwise the route handler).
    // return next.handle();

    return next.handle().pipe(
      tap(() => {
        // this.loggerService.log('throw the error');
        // throw new Error('some error');
      }),
      catchError((err: Error) => {
        this.loggerService.descPad();
        this.loggerService.log('Interceptor1Interceptor => catchError =>', err);
        return throwError(new HttpException(err.message, HttpStatus.ACCEPTED));
      }),
      tap(data => {
        this.loggerService.logTapTypeof(data);
        // this.loggerService.log(
        //   'Tapping into Interceptor1Interceptor => next: CallHandler => handle() => Observable<Response<T>>',
        // );
      }),
      tap(() => {
        this.loggerService.log('next up, mapping the data...');
      }),
      map(data => ({ data })),
      tap(data => {
        this.loggerService.log('mapped data =>', data);
      }),
      tap(() => {
        this.loggerService.log('END Interceptor1Interceptor');
      }),
      tap(() => {
        this.loggerService.descPad();
      }),
    );
  }
}
