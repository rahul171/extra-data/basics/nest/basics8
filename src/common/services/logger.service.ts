import { Injectable } from '@nestjs/common';
import { PadService } from './pad.service';

@Injectable()
export class LoggerService {
  constructor(private padService: PadService) {
    console.log('LoggerService => constructor');
  }

  log(...args) {
    const pad = this.padService.getPad();
    this.logIt(args, pad);
  }

  private logIt(args, pad = '') {
    if (pad.length > 0) {
      console.log(pad, ...args);
    } else {
      console.log(...args);
    }
  }

  logTapTypeof(obj) {
    this.log('data =>', typeof obj, '=>', obj);
  }

  logWithoutPad(...args) {
    this.logIt(args);
  }

  incPad() {
    this.padService.incPad();
  }

  descPad() {
    this.padService.descPad();
  }

  resetPad() {
    this.padService.resetPad();
  }
}
