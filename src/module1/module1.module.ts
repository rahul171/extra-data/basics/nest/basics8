import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { Module1Service } from './module1.service';
import { Module1Controller } from './module1.controller';
import { LoggerService } from '../common/services/logger.service';
import { PadService } from '../common/services/pad.service';
import { ResetPadMiddleware } from '../common/middlewares/reset-pad.middleware';

@Module({
  providers: [Module1Service, LoggerService, PadService],
  controllers: [Module1Controller],
})
export class Module1Module implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ResetPadMiddleware).forRoutes(Module1Controller);
  }
}
