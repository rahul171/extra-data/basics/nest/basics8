import {
  Controller,
  Get,
  UseFilters,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Module1Service } from './module1.service';
import { Interceptor1Interceptor } from '../common/interceptors/interceptor1.interceptor';
import { Interceptor2Interceptor } from '../common/interceptors/interceptor2.interceptor';
import { LoggerService } from '../common/services/logger.service';
import { Interceptor3Interceptor } from '../common/interceptors/interceptor3.interceptor';
import { Interceptor4Interceptor } from '../common/interceptors/interceptor4.interceptor';

@Controller('module1')
export class Module1Controller {
  constructor(
    private module1Service: Module1Service,
    private loggerService: LoggerService,
  ) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Get('route1')
  @UseInterceptors(
    Interceptor1Interceptor,
    Interceptor2Interceptor,
    Interceptor4Interceptor,
    Interceptor3Interceptor,
  )
  route1(): string {
    this.loggerService.incPad();
    this.loggerService.log('Module1Controller => route1()');
    // throw new Error('from the route1()');
    this.loggerService.descPad();
    return 'route1';
  }
}
