import { Injectable } from '@nestjs/common';
import { LoggerService } from '../common/services/logger.service';

@Injectable()
export class AppService {
  getMessage(): string {
    return 'welcome to the app service';
  }
}
