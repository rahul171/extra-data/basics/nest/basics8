import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { Module1Module } from '../module1/module1.module';
import { ResetPadMiddleware } from '../common/middlewares/reset-pad.middleware';
import { Module1Controller } from '../module1/module1.controller';

@Module({
  imports: [Module1Module],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
