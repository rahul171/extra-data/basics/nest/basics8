import { Injectable } from '@nestjs/common';

@Injectable()
export class PadService {
  private pad: number = 0;
  private padSize: number = 4;
  private padChar: string = '-';

  constructor() {
    console.log('PadService => constructor');
  }

  getPad() {
    let str = '';
    for (let i = 0; i < this.pad; i++) {
      str += this.padChar;
    }
    return str;
  }

  incPad() {
    this.pad += this.padSize;
  }

  descPad() {
    this.pad -= this.padSize;
    this.adjustUnderflow();
  }

  private adjustUnderflow() {
    if (this.pad < 0) {
      this.pad = 0;
    }
  }

  resetPad() {
    this.pad = 0;
  }
}
