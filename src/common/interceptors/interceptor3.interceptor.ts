import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { LoggerService } from '../services/logger.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class Interceptor3Interceptor implements NestInterceptor {
  constructor(private loggerService: LoggerService) {}

  async intercept(context: ExecutionContext, next: CallHandler) {
    this.loggerService.incPad();
    this.loggerService.log('Interceptor3Interceptor => async intercept()');

    const t = 5;
    this.loggerService.log(`waiting ${t} seconds`);

    await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, t * 1000);
    });

    this.loggerService.logWithoutPad(
      `Interceptor3Interceptor => finished waiting ${t} seconds`,
    );

    return next.handle().pipe(
      tap(data => {
        this.loggerService.logTapTypeof(data);
      }),
      tap(() => {
        this.loggerService.log('END Interceptor3Interceptor');
      }),
      tap(() => {
        this.loggerService.descPad();
      }),
    );
  }
}
